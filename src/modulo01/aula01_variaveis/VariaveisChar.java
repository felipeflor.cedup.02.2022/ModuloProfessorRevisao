package modulo01.aula01_variaveis;

public class VariaveisChar {

	public static void main(String[] args) {
		
		char oC = 'o';
		char iC = 'i';
		
		char o = 111;
		char i = 105;
		char exclamacao = 0x21; // valor '?'
		
		System.out.println(oC);
		System.out.println(iC);
		System.out.println(oC+iC);
		System.out.println(""+oC+iC);

		System.out.println();

		System.out.println(o);
		System.out.println(i);
		System.out.println(exclamacao);
		System.out.println(o+i+exclamacao);
		System.out.println(""+o+i+exclamacao);

	}

}
