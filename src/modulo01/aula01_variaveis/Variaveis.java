package modulo01.aula01_variaveis;

public class Variaveis {

    public static void main(String[] args) {

        //Declare inicie com seus dados uma variavel nome
        String nome = "Clayton Andrade";

        //Declare e inicie com seus dados uma variavel idade com valor 18
        int idade = 18;

        //Declare inicie com seus dados uma variavel cidadeNatal
        String cidadeNatal = "Içara";

        //Declare inicie com seus dados uma variavel ano
        String ano2023 = "2023";

        //Imprima na tela a mensagem "Idade = " + a variavel idade + "\n"
        System.out.println("Idade = " + idade + "\n");

        //Altere o valor da variavel idade
        idade = 36;

        //Imprima na tela a mensagem "Nome = " + a variavel nome
        System.out.println("Nome = " + nome);

        //Imprima na tela a mensagem "Idade = " + a variavel idade
        System.out.println("Idade = " + idade);

        //Imprima na tela a mensagem "Cidade Natal = " + a variavel cidade natal
        System.out.println("Cidade Natal = " + cidadeNatal);

        //Imprima na tela a mensagem "Ano = " + a variavel ano
        System.out.println("Cidade Ano = " + ano2023);
    }
}
